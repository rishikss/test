function SearchCtrl($scope, $http) {
	
	// The function that will be executed on change keyword search
	$scope.keyword_change = function(){
		console.error("Search string changed........");
	    
		$http({method: 'GET', url: 'https://api.spotify.com/v1/search',params:{q: $scope.keywords, type: 'artist'}}).
		success(function(data, status, headers, config) {
		  // this callback will be called asynchronously
		  // when the response is available
			$scope.data = data;	
			$scope.result = data.artists.items;
			console.log('Artist information 123456', data.artists.items);
		}).
		error(function(data, status, headers, config) {
		  // called asynchronously if an error occurs
		  // or server returns response with an error status.
		});
	};
	//
	$scope.chk_album = function(item){
		alert(item.id);
		$http({method: 'GET', url: 'https://api.spotify.com/v1/artists/'+item.id+'/albums'}).
		success(function(data, status, headers, config) {
		  // this callback will be called asynchronously
		  // when the response is available
			$scope.data = "";	
			$scope.result = "";
			$scope.albums = data.items;
			console.log('Album data', data);
		}).
		error(function(data, status, headers, config) {
		  // called asynchronously if an error occurs
		  // or server returns response with an error status.
		});
	};
	//
	$scope.chk_track = function(item){
		$http({method: 'GET', url: 'https://api.spotify.com/v1/albums/'+item.id+'/tracks'}).
		success(function(data, status, headers, config) {
		  // this callback will be called asynchronously
		  // when the response is available
			$scope.data = "";	
			$scope.result = "";
			$scope.albums = "";
			$scope.tracks = data.items;
			console.log('Track data', data);
		}).
		error(function(data, status, headers, config) {
		  // called asynchronously if an error occurs
		  // or server returns response with an error status.
		});
	};
}
